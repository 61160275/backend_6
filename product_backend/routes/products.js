const express = require('express')
// const res = require('express/lib/response')
const router = express.Router()
const Product = require('../models/Product') // ออก 1 ทีแล้วอยู่ข้างนอกแล้วก็เข้า models ไปอีก 1 ที
// const products = [
//   { id: 1, name: 'IPad gen1 64G wifi', price: 11000.00 },
//   { id: 2, name: 'IPad gen2 64G wifi', price: 12000.00 },
//   { id: 3, name: 'IPad gen3 64G wifi', price: 13000.00 },
//   { id: 4, name: 'IPad gen4 64G wifi', price: 14000.00 },
//   { id: 5, name: 'IPad gen5 64G wifi', price: 15000.00 },
//   { id: 6, name: 'IPad gen6 64G wifi', price: 16000.00 },
//   { id: 7, name: 'IPad gen7 64G wifi', price: 17000.00 },
//   { id: 8, name: 'IPad gen8 64G wifi', price: 18000.00 },
//   { id: 9, name: 'IPad gen9 64G wifi', price: 19000.00 },
//   { id: 10, name: 'IPad gen10 64G wifi', price: 20000.00 }
// ]
// const lastId = 11
const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getProduct = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const product = await Product.findById(id).exec() // await ให้รอ
    if (product === null) {
      res.status(404).json({
        message: 'Product not found!!!'
      })
    }
    res.json(product)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}
const addProducts = async function (req, res, next) { // มี await ก็ต้องมี async และ try catch
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
  res.status(201).json(newProduct) // ต้องส่งเป็นแบบนี้ถ้าส่ง req.body ไป id จะเป็น -1
}

const updateProduct = async function (req, res, next) { // แก้ไขสินค้า
  const productId = req.params.id // เอาข้อมูลเข้าไปเก็บในตัวแปร
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteProduct = async function (req, res, next) { // ลบสินค้า
  const productId = req.params.id// เอาข้อมูลเข้าไปเก็บในตัวแปร
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
router.get('/', getProducts) // GET Products มาทั้งหมด
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // Add New Product
router.put('/:id', updateProduct) // Edit Product
router.delete('/:id', deleteProduct) // delete Product
module.exports = router
