const express = require('express')
const router = express.Router()
const User = require('../models/User') // ออก 1 ทีแล้วอยู่ข้างนอกแล้วก็เข้า models ไปอีก 1 ที
const { generateAccessToken } = require('../helpers/auth')
const bcrypt = require('bcryptjs')
const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password

  try {
    const user = await User.findOne({ username: username }).exec() // await ให้รอ
    const verifyResult = await bcrypt.compare(password, user.password)
    if (!verifyResult) {
      res.status(404).json({
        message: 'User not found!!!'
      })
    }
    const token = generateAccessToken({ _id: user._id, username: user.username })
    res.json({ user: { _id: user._id, username: user.username, roles: user.roles }, token: token })
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.post('/login', login) // Add New User
module.exports = router
