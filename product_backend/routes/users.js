const express = require('express')
const router = express.Router()
const User = require('../models/User') // ออก 1 ทีแล้วอยู่ข้างนอกแล้วก็เข้า models ไปอีก 1 ที

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getUser = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const user = await User.findById(id).exec() // await ให้รอ
    if (user === null) {
      res.status(404).json({
        message: 'User not found!!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}
const addUsers = async function (req, res, next) { // มี await ก็ต้องมี async และ try catch
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
  res.status(201).json(newUser) // ต้องส่งเป็นแบบนี้ถ้าส่ง req.body ไป id จะเป็น -1
}

const updateUser = async function (req, res, next) { // แก้ไขสินค้า
  const UserId = req.params.id // เอาข้อมูลเข้าไปเก็บในตัวแปร
  try {
    const user = await User.findById(UserId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteUser = async function (req, res, next) { // ลบสินค้า
  const userId = req.params.id// เอาข้อมูลเข้าไปเก็บในตัวแปร
  try {
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
router.get('/', getUsers) // GET Users มาทั้งหมด
router.get('/:id', getUser) // GET One User
router.post('/', addUsers) // Add New User
router.put('/:id', updateUser) // Edit User
router.delete('/:id', deleteUser) // delete User
module.exports = router
