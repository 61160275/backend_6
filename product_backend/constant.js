const ROLE = {
  ADMIN: 'ADMIN',
  LOCAL_ADMIN: 'LOCAL_ADMIN',
  USER: 'USER'
}

module.exports = { // ใช้ปีกกาเพราะออกมาเป็น object
  ROLE
}
